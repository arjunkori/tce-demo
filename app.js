var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var routes = require('./routes/index');
var users = require('./routes/users');
var nongsm = require('./routes/nongsm');

var app = express();
var mongoose = require('mongoose');

//let sess;
global.sess;

app.set('port', 3000);
var server = app.listen(app.get ('port'), function() {
    var host = server.address().address
    var port = server.address().port
    console.log('Server listening at http: ' + host + " " + port);
});
// configuration ===============================================================
mongoose.connect('mongodb://tce-user:tce123@ds151431.mlab.com:51431/tce-demo', function(err, db) {
  if(!err) {
    console.log("MONGO CONNECTED");
  } else {
      console.log("ERROR MONGO  :: "+err);
  }
});// connect to our database

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile); 
app.set('view engine', 'html');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/')));

app.use('/', routes);
app.use('/users', users);
app.use('/nongsm', nongsm);

var now = new Date(); //SESSION TIME toUTCstring()
var time = now.getTime();
var expireTime = time + 100*18000;
now.setTime(expireTime);
var _maxAge = new Date(Date.now() + 1800000);
app.use(session({
  secret: 'ssshhhhh',
  saveUninitialized: true,
  resave: false,  
  cookie:{
      maxAge: _maxAge,
      expires: new Date(Date.now() + _maxAge) ,
      path:'/',
      rolling:true
    }
})
);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
