var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var schoolSchema = new Schema({
    sap_customer_code: { type: String},
    school_name: { type: String},
    location: { type: String},
    state: { type: String},
    zone: { type: String},
    tier: { type: String},
    legal_status: { type: String},
    customer_address: { type: String},
    techical_manager: { type: String},
    academic_manager: { type: String},
    mancom_manager: { type: String},
    techincal_manager_email_address: { type: String},
    academic_manager_email_address: { type: String},
    mancom_manager_email_address: { type: String},
    relationship_manager: { type: String},
    relationship_manager_email_address: { type: String},
    sales_manager: { type: String},
    sales_manager_email_address: { type: String},
    srt_director: { type: String},
    srt_director_email_address: { type: String},
    customer_segmentation: { type: String},
    school_type: { type: String}
});


var School = mongoose.model('school', schoolSchema);

module.exports = School;
