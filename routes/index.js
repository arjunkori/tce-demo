"use strict";

var express = require('express');

//Converter Class 
var Converter = require("csvtojson").Converter;
var converter = new Converter({});
var School = require('../models/school');
var Contract = require('../models/contract');

const schoolSchema = require('../models/school');
var router = express.Router();

/* Upload Excelfile page. */
router.post('/upload/customer', function(req, res, next) {
 try{                    
     
        const csvFilePath='../school_dump.csv'
        const csv=require('csvtojson')
        csv()
        .fromFile(csvFilePath)
        .on('json',(jsonObj)=>{
            console.log("********************************** CSV ****************************"+ util.inspect(jsonObj));
            // combine csv header row and csv line to a json object 
            // jsonObj.a ==> 1 or 4 
            var school = new School({
                sap_customer_code: jsonObj.schoolId,
                school_name: jsonObj.schoolName,
                location: jsonObj.Location,
                state: jsonObj.State,
                zone: jsonObj.Zone,
                tier: jsonObj.Tier,
                legal_status: jsonObj['Legal Status'],
                customer_address: jsonObj.Address,
                techical_manager: jsonObj.technicalManager,
                academic_manager: jsonObj.academicManager,
                mancom_manager: jsonObj.mancomMember,
                techincal_manager_email_address: jsonObj.technicalManagerEmail,
                academic_manager_email_address: jsonObj.academicManagerEmail,
                mancom_manager_email_address: jsonObj.mancomMemberEmail,
                relationship_manager: jsonObj.relationshipManager,
                relationship_manager_email_address: jsonObj.relationshipManagerEmail,
                sales_manager: jsonObj.salesManager,
                sales_manager_email_address: jsonObj.salesManagerEmail,
                srt_director: jsonObj.srtDirector,
                srt_director_email_address: jsonObj.srtDirectorEmail,
                customer_segmentation: jsonObj['Customer Segmentation'],
                school_type: jsonObj.schoolType
            });
            school.save(function(err, data) {
                            if (err) {                         
                                if(err.code == 11000){
                                    res.send('Duplicate Amenities FOUND');
                                } else {
                                    res.send('Unknow Error : '+ JSON.stringify(err));
                                }
                            } else { 
                                if(data)
                                    console.log("RECORD INSERTED :"+ data);
                                else {
                                    console.log("Unable to insert data");
                                }
                            }
            });
        })
        .on('done',(error)=>{
            console.log('end')
        })
 } catch(ex) {
      console.log("EXCEPTION :: "+ JSON.stringify(ex));
 }
});


router.post('/upload/contract', function(req, res, next) {
 try{                    
     
        const csvFilePath='./contract.csv'
        const csv=require('csvtojson')
        csv()
        .fromFile(csvFilePath)
        .on('json',(jsonObj)=>{
            console.log("********************************** CSV ****************************"+ util.inspect(jsonObj));
            // combine csv header row and csv line to a json object 
            // jsonObj.a ==> 1 or 4 
            var contract = new Contract({
                id : jsonObj.ID,
                school_id  : jsonObj.schoolId,
                //agreement_sr_no : jsonObj.,
                new_repeat : jsonObj['New/Upsell'],
                contact_no : "",
                board_type : "",
                
                email_id : jsonObj.technicalManagerEmail,
                period_of_signing : "",
                efective_date : "",
                commencement_date : "",
                expiry_date : "",
                no_of_school : "",
                no_of_classroom : jsonObj.numberOfClassrooms,
                contact_person : "",
                department_designation : "",
                sales_officer : "",
                sales_manager : jsonObj.salesManager,
                director_1 : jsonObj.srtDirector,
                director_2 : "",
                senior_manager : "",
                value_of_the_agreement : "",
                quaterly_payment_for_1st_year : "",
                quaterly_payment_for_rest_4th_year : "",
                monthly : "",
                product : "",
                ec_tenure_in_months : "",
                contract_tenure_in_yrs : "",
                hadware_in_rs : "",
                content_in_rs : "",
                support_in_rs : "",
                hadware_in_rs_monthly : "",
                content_in_rs_monthly : "",
                support_in_rs_montly : "",
                hardware : "",
                classedge_content : "",
                edge_co_ordinator : "",
                projector : "",
                kyc_status : "",
                scanned_copy_revised : "",
                registration_no_of_the_school : "",
                trust_society_name : "",
                pan_no : "",
                bdm_emp : "",
                vpn : "",
                delivery_trigger_date : "",
                submitted_to_legal_on : "",
                actual_go_live : jsonObj['Go Live Date'],
                legal_status : jsonObj['Legal Status'],
                agreement_status : "",
                cr_as_legal : "",
                school_name_as_per_legal : "",
                reconciliation : "",
                pdc_wavier : "",
                pdc_recevied : "",
                contract_no : jsonObj.ContractNo,
                content_pm_taxes : "",
                content_pcpm_taxes : "",
                poa_approval_taken : ""
            });
            contract.save(function(err, data) {
                            if (err) {                         
                                if(err.code == 11000){
                                    res.send('Duplicate Amenities FOUND');
                                } else {
                                    res.send('Unknow Error : '+ JSON.stringify(err));
                                }
                            } else { 
                                if(data)
                                    console.log("RECORD INSERTED :"+ data);
                                else {
                                    console.log("Unable to insert data");
                                }
                            }
            });
        })
        .on('done',(error)=>{
            console.log('end')
        })
 } catch(ex) {
      console.log("EXCEPTION :: "+ JSON.stringify(ex));
 }
});


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Express'
    });
});

/**
 * ajax call to validate request
 */

//#################################################### TATA

router.get('/schoolList',function(req,res){
    schoolSchema.find().limit(200).lean().exec().then(function (data) {
        console.log('####### Schools ',data);
        res.send(data)
    });

});

router.post('/schoolDetails',function(req,res){
    let schoolid = req.body.cname;
    
    schoolSchema.find({'sap_customer_code':schoolid}).limit(1).lean().exec().then(function(result){
        //console.log(result);
        res.render('schhol_detail',{'data':result});        
    }).catch(function(err){
        res.send(err);
    })
});

router.get('/contractDetail',function(req,res){
  var id = req.param.id;
  //Contract.find({})
      Contract.find({'school_id':id}).limit(1).lean().exec().then(function(result){
        console.log("############# CONTRACT DETAIL ",result);
        res.render('contract_detail',{'data':result});        
    }).catch(function(err){
        res.send(err);
    })

    //res.render('contract_detail',{data:data});
})
module.exports = router;