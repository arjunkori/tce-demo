"use strict";

var express = require('express');
const request = require('request-promise');
var xml2js = require('xml2js');
const Promise = require('bluebird');
const util = require('util');


var router = express.Router();
let parser = new xml2js.Parser();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index2', {
        title: 'Express'
    });
});

router.post('/validate', function(req, res) {
    const srno = req.body.srno;
    const cname = req.body.cname;
    const model = req.body.model;

    console.log(req.body);
    console.log('inside non gsm validate', srno, cname,model);

    let options = {
        url: "http://202.154.175.12:1024/service.asmx/checkSrNoResult",
        //url:"http://202.154.175.12:1024/service.asmx/GetSerialInfo?name=LGtest&serialnumber=352136060669658",
        qs: {
            'name': cname,
            'SR_NO': srno,
            'model':model
        }
    }

    request(options).then(function(response) {
        console.log('WS Response is : ', response, typeof(response));

        let parserAsync = Promise.promisify(parser.parseString);
        let con = Promise.promisifyAll(connection)
        parserAsync(response).then(function(result) {
            //insert into mysql 

            let serial_data = {
                serialnumber: srno,
                name: cname,
                status: result['SerialValidate']['Data'][0]['STATUS'][0],
                statuscode: result['SerialValidate']['Data'][0]['STATUSCODE'][0],
                model:model
                //timestamp:new Date()
            }

            let comma = ",";
            con.queryAsync('INSERT INTO serial_no_non_gsm(serialnumber,name,status,statuscode,model) VALUES  ("' + serial_data.serialnumber + '"' + comma + '"' + serial_data.name + '"' + comma + '"' + serial_data.status + '"' + comma + '"' + serial_data.statuscode + '"' + comma + '"' + serial_data.model + '")').then(function(mysqlResponse) {
                console.log('########### MYSQL NON GSM RESPONSE ############ ', serial_data);
                res.send(serial_data);
            }).catch(function(err) {
                console.log('Erro in mysql ', err)
            })
        }).catch(function(err) {
            res.send({
                message: 'Error occured'
            })
        })
    }).catch(function(err) {
        console.log('Error in URL ', err)
    })
});

router.get('/list',function(req,res){
    connection.query('SELECT * FROM serial_no_non_gsm',function(err,rows,fields){
        console.log(rows,fields,typeof(rows));
        
        res.render('nonGsmProduct',{'data':rows})
    })
});

module.exports = router;
